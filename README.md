# About

A songbook written in LaTeX. Uses the awesome
[songs package](http://songs.sourceforge.net/songs.pdf).